from pathlib import Path

from modules import helpers
from modules import report_builder


def main() -> int:
    directory = Path("Daily Reports").resolve()
    files = helpers.list_files("Report", directory)
    # Next, for each blank cell in range, copy value above (merged cells end up blank)
    report_builder.merge_bloom_placements(files)

    return 0


if __name__ == "__main__":
    main()